# Fipe API

### Descrição

Esta API está implementada seguindo a especificação `{json:api} v1.0` que pode ser encontrada
[aqui](https://jsonapi.org/).

### Instalação

Este é um projeto feito com o framework [Lumen](https://lumen.laravel.com/), veja
informações de [instalação](https://lumen.laravel.com/docs/8.x/installation) 
no proprio site do framework.

Para instalar as dependências para produção, use:
```shell
$ composer install -o --no-dev
```

Para teste localmente, use:
```shell
$ composer serve
```

### Palavras chave:
- _Listar_: Significa que o endpoint irá retornar um `array` em seu `data`
- _Obter_: Significa que o endpoint irá retornar um `objeto` em seu `data`

### Retorno padrão:
Caso o recurso pesquisado não exista, a api irá retornar
um erro `404`:

```json
{
    "message": "..."
}
```

### Assinatura de objetos da API (Resources)

- Marca
```typescript
interface Brand {
    id: String,
    name: String
}
```

- Modelo
```typescript
interface Model {
    id: Number,
    name: String
}
```

- Preço do Veículo
```typescript
interface VehiclePrice {
    id: String,
    value: Number,
    valueBRL: String,
    fipeCode: String,
    vehicleTypeId: Number,
    queryDate: String
}
```

- Tipo de Veículo
```typescript
interface VehicleType {
    id: Number,
    name: String,
    slug: String,
}
```

- Ano
```typescript
interface Year {
    id: String,
    name: String,
    year: String,
    fuelTypeId: Number
}
```

- FuelType
```typescript
interface FuelType {
    id: Number,
    name: String,
    acronym: String
}
```

### Dica:
Em cada endpoint está uma representação typescript do tipo de retorno da api,
você pode usar para pesquisar rapidamente o endpoint que precisa usando
o `Ctrl + F` ou `⌘ + F`, por exemplo:

Caso um endpoint retorne uma listagem, você pode dar um `Ctrl + F` ou `⌘ + F` por:
```typescript
Promise<Array<_NomeDoResource_>>
```

Caso um endpoint retorne uma único objeto você pode dar um `Ctrl + F` ou `⌘ + F` por:
```typescript
Promise<_NomeDoResource_>
```

### Limitações:
- Não possui filtros, sorts, includes, fields, etc.
- Não possui create, update ou delete.
- Cache apenas no client-side. (Cache Control)
- Resource Links: Apenas o `self`

---
## Referência da API:

[Listar] todos os tipos de veículos:
- 

- GET

```
/v1/vehicle-types
```

- Retorno (200)

```json
{
    "data": [
        {
            "type": "VehicleType",
            "id": 1,
            "attributes": {
                "name": "Carros",
                "slug": "cars"
            }
        },
        ...
    ],
    "links": {...}
}
  ```

- Representação
```typescript
Promise<Array<VehicleType>>
```

[Listar] marcas de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands
```

- Retorno (200)

```json
{
    "data": [
        {
            "type": "Brand",
            "id": "1",
            "attributes": {
                "name": "Acura"
            },
            "links": {...}
        },
        ...
    ],
    "links": {...}
}
  ```

- Representação
```typescript
Promise<Array<Brand>>
```


[Obter] marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId
```

- Retorno (200)

```json
{
    "data": {
        "type": "Brand",
        "id": "1",
        "attributes": {
            "name": "Acura"
        },
        "links": {...}
    }
}
  ```

- Representação
```typescript
Promise<Brand>
```


[Listar] modelos de um marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/models
```

- Retorno (200)

```json
{
    "data": [
        {
            "type": "Model",
            "id": 1,
            "attributes": {
                "name": "Integra GS 1.8"
            },
            "links": {...}
        },
        ...
    ],
    "links": {...}
}
  ```

- Representação
```typescript
Promise<Array<Model>>
```


[Obter] modelos de um marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/models/:modelId
```

- Retorno (200)

```json
{
    "data": {
        "type": "Model",
        "id": 1,
        "attributes": {
            "name": "Integra GS 1.8"
        },
        "links": {...}
    }
}
  ```

- Representação
```typescript
Promise<Model>
```


[Listar] anos de uma marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/years
```

- Retorno (200)

```json
{
    "data": [
        {
            "type": "Year",
            "id": "1998-1",
            "attributes": {
                "name": "1998 Gasolina",
                "year": "1998",
                "fuelTypeId": "1"
            },
            "links": {...}
        },
        ...
    ],
    "links": {...}
}
  ```

- Representação
```typescript
Promise<Array<Year>>
```


[Obter] ano de uma marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/years/:yearId
```

- Retorno (200)

```json
{
    "data": {
        "type": "Year",
        "id": "1998-1",
        "attributes": {
            "name": "1998 Gasolina",
            "year": "1998",
            "fuelTypeId": "1"
        },
        "links": {...}
    }
}
  ```

- Representação
```typescript
Promise<Year>
```



[Listar] modelos de um ano específico de uma marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/years/:yearId/models
```

- Retorno (200)

```json
{
    "data": [
        {
            "type": "Model",
            "id": "1",
            "attributes": {
                "name": "Integra GS 1.8"
            },
            "links": {...}
        },
        ...
    ],
    "links": {...}
}
  ```

- Representação
```typescript
Promise<Array<Model>>
```



[Obter] modelo de um ano específico de uma marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/years/:yearId/models
```

- Retorno (200)

```json
{
    "data": {
        "type": "Model",
        "id": "1",
        "attributes": {
            "name": "Integra GS 1.8"
        },
        "links": {...}
    }
}
  ```

- Representação
```typescript
Promise<Model>
```



[Obter] preço de um modelo específico de um ano específico de uma marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/years/:yearId/models/:modelId/price
```

- Retorno (200)

```json
{
    "data": {
        "type": "VehiclePrice",
        "id": "hqrdwnb5yk",
        "attributes": {
            "valueBRL": "R$ 14.279,00",
            "fipeCode": "006008-9",
            "vehicleTypeId": 1,
            "queryDate": "segunda-feira, 16 de agosto de 2021 23:43",
            "value": 14279
        },
        "relationships": {
            "brand": {...},
            "year": {...},
            "model": {...},
            "vehicleType": {...},
            "fuelType": {...}
        }
    },
    "links": {...}
}
  ```

- Representação
```typescript
Promise<VehiclePrice>
```



[Listar] anos de um modelo específico de uma marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/models/:modelId/years
```

- Retorno (200)

```json
{
    "data": [
        {
            "type": "Year",
            "id": "1992-1",
            "attributes": {
                "name": "1992 Gasolina",
                "year": "1992",
                "fuelTypeId": "1"
            },
            "links": {...}
        },
        ...
    ],
    "links": {...}
}
  ```

- Representação
```typescript
Promise<Array<Year>>
```



[Obter] ano de um modelo específico de uma marca específica de um tipo de veículo:
- 

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/models/:modelId/years/:yearId
```

- Retorno (200)

```json
{
    "data": {
        "type": "Year",
        "id": "1992-1",
        "attributes": {
            "name": "1992 Gasolina",
            "year": "1992",
            "fuelTypeId": "1"
        },
        "links": {...}
    }
}
  ```

- Representação
```typescript
Promise<Year>
```

## [Obter] preço de um ano específico de um modelo espeçífico de uma marca específica de um tipo de veículo.

- GET

```
/v1/vehicle-types/:vehicleTypeId/brands/:brandId/models/:modelId/years/:yearId/price
```

- Retorno (302)

```json
{
    "data": {
        "type": "VehiclePrice",
        "id": "hqrdwnb5yk",
        "attributes": {
            "valueBRL": "R$ 14.279,00",
            "fipeCode": "006008-9",
            "vehicleTypeId": 1,
            "queryDate": "segunda-feira, 16 de agosto de 2021 23:43",
            "value": 14279
        },
        "relationships": {
            "brand": {...},
            "year": {...},
            "model": {...},
            "vehicleType": {...},
            "fuelType": {...}
        }
    },
    "links": {...}
}
```

- Representação

```typescript
Promise<VehiclePrice>
```
