<?php

namespace App\Traits;

use App\Http\Clients\Fipe\VehiclesClient;
use App\Resources\External\VehiclePrice;

/**
 * Trait InteractsWithVehicles.
 *
 * @author ErickJMenezes <erickmenezes.dev@gmail.com>
 * @mixin \App\Http\Controllers\Controller
 */
trait InteractsWithVehicles
{
    public function getVehiclePrice(
        string $vehicleTypeId,
        string $brandId,
        string $yearId,
        string $modelId,
    ): VehiclePrice
    {
        $this->abortIfInvalidVehicleType($vehicleTypeId);

        [$modelYear, $fuelTypeId] = explode('-', $yearId);

        $data = app(VehiclesClient::class)->getVehicleValue(
            $this->getLatestReferenceTableId(),
            $vehicleTypeId,
            $brandId,
            $yearId,
            $fuelTypeId,
            $modelYear,
            $modelId,
        );

        $this->abortIfNothingFound(collect($data));

        return VehiclePrice::make(array_merge($data, [
            'yearId' => $yearId,
            'brandId' => $brandId,
            'modelId' => $modelId
        ]));
    }
}
