<?php

namespace App\Http\Clients\Fipe;

use Waffler\Waffler\Attributes\Request\JsonParam;
use Waffler\Waffler\Attributes\Utils\Unwrap;
use Waffler\Waffler\Attributes\Verbs\Post;

/**
 * Interface VehiclesClient.
 *
 * Serviço para integrar com a api não oficial do Fipe.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Clients\Fipe
 */
interface VehiclesClient
{
    /**
     * Obtém a lista de tabelas de referência.
     *
     * @return array
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    #[Post('ConsultarTabelaDeReferencia')]
    public function getReferenceTable(): array;

    /**
     * Obtém as marcas pelo código de veículo
     *
     * @param int $tableCode
     * @param int $vehicleType
     *
     * @return array
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    #[Post('ConsultarMarcas')]
    public function getBrands(
        #[JsonParam('codigoTabelaReferencia')] int $tableCode,
        #[JsonParam('codigoTipoVeiculo')] int $vehicleType,
    ): array;

    /**
     * Obtém a lista de modelos de um tipo de veículo de uma marca específica.
     *
     * @param int $tableCode
     * @param int $vehicleType
     * @param int $brandCode
     *
     * @return array
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    #[Post('ConsultarModelos')]
    #[Unwrap('Modelos')]
    public function getModels(
        #[JsonParam('codigoTabelaReferencia')] int $tableCode,
        #[JsonParam('codigoTipoVeiculo')] int $vehicleType,
        #[JsonParam('codigoMarca')] int $brandCode,
    ): array;

    /**
     * Obtém a lista de anos de um tipo de veículo de uma marca específica.
     *
     * @param int $tableCode
     * @param int $vehicleType
     * @param int $brandCode
     *
     * @return array
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    #[Post('ConsultarModelos')]
    #[Unwrap('Anos')]
    public function getYears(
        #[JsonParam('codigoTabelaReferencia')] int $tableCode,
        #[JsonParam('codigoTipoVeiculo')] int $vehicleType,
        #[JsonParam('codigoMarca')] int $brandCode,
    ): array;

    /**
     * Obém o ano de um modelo de uma marca específica.
     *
     * @param int $tableCode
     * @param int $vehicleType
     * @param int $brandCode
     * @param int $modelCode
     *
     * @return array
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    #[Post('ConsultarAnoModelo')]
    public function getModelYears(
        #[JsonParam('codigoTabelaReferencia')] int $tableCode,
        #[JsonParam('codigoTipoVeiculo')] int $vehicleType,
        #[JsonParam('codigoMarca')] int $brandCode,
        #[JsonParam('codigoModelo')] int $modelCode,
    ): array;

    /**
     * Consulta modelos de um ano específico.
     *
     * @param int    $tableCode
     * @param int    $vehicleType
     * @param int    $brandCode
     * @param string $year
     * @param int    $fuelTypeCode
     * @param int    $modelYear
     *
     * @return array
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    #[Post('ConsultarModelosAtravesDoAno')]
    public function getModelsByYear(
        #[JsonParam('codigoTabelaReferencia')] int $tableCode,
        #[JsonParam('codigoTipoVeiculo')] int $vehicleType,
        #[JsonParam('codigoMarca')] int $brandCode,
        #[JsonParam('ano')] string $year,
        #[JsonParam('codigoTipoCombustivel')] int $fuelTypeCode,
        #[JsonParam('anoModelo')] int $modelYear,
    ): array;

    /**
     * Obém a lista de valores de um veículo.
     *
     * @param int    $tableCode
     * @param int    $vehicleType
     * @param int    $brandCode
     * @param string $year
     * @param int    $fuelTypeCode
     * @param int    $modelYear
     * @param int    $modelCode
     * @param string $consultType
     *
     * @return array
     * @author ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    #[Post('ConsultarValorComTodosParametros')]
    public function getVehicleValue(
        #[JsonParam('codigoTabelaReferencia')] int $tableCode,
        #[JsonParam('codigoTipoVeiculo')] int $vehicleType,
        #[JsonParam('codigoMarca')] int $brandCode,
        #[JsonParam('ano')] string $year,
        #[JsonParam('codigoTipoCombustivel')] int $fuelTypeCode,
        #[JsonParam('anoModelo')] int $modelYear,
        #[JsonParam('codigoModelo')] int $modelCode,
        #[JsonParam('tipoConsulta')] string $consultType = 'tradicional',
    ): array;
}
