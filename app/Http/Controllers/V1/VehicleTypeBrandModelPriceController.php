<?php

namespace App\Http\Controllers\V1;

use App\Http\Clients\Fipe\VehiclesClient;
use App\Http\Controllers\Controller;
use App\Traits\InteractsWithVehicles;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class VechicleTypeBrandModelPriceController.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Http\Controllers\V1
 */
class VehicleTypeBrandModelPriceController extends Controller
{
    use InteractsWithVehicles;

    public function index(
        string $vehicleTypeId,
        string $brandId,
        string $modelId,
        VehiclesClient $client,
    ): JsonResponse
    {
        $fipeTableId = $this->getLatestReferenceTableId();

        $years = $client->getModelYears(
            $fipeTableId,
            $vehicleTypeId,
            $brandId,
            $modelId
        );

        $prices = collect();

        foreach ($years as $year) {
            $prices->push($this->getVehiclePrice(
                $vehicleTypeId,
                $brandId,
                $year['Value'],
                $modelId
            ));
        }

        return response()->json([
            'data' => $prices->toArray(),
            'links' => [
                'self' => request()->getUri(),
            ]
        ]);
    }
}
