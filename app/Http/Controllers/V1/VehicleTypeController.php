<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Resources\External\VehicleTypeTransformer;
use Illuminate\Http\JsonResponse;

/**
 * Class VehicleTypeController.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Http\Controllers\V1
 */
class VehicleTypeController extends Controller
{
    public function index(): JsonResponse
    {
        $types = collect([['id' => 1], ['id' => 2], ['id' => 3]])
            ->map(fn ($type) => new VehicleTypeTransformer($type));

        return response()->json([
            'data' => $types,
            'links' => [
                'self' => request()->getUri()
            ]
        ]);
    }
}
