<?php

namespace App\Http\Controllers\V1;

use App\Http\Clients\Fipe\VehiclesClient;
use App\Http\Controllers\Controller;
use App\Resources\External\BrandTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class VehicleTypeBrandsController extends Controller
{
    public function index(string $vehicleTypeId, VehiclesClient $vehiclesClient): JsonResponse
    {
        $brands = $this->getBrands($vehicleTypeId, $vehiclesClient);

        return response()->json([
            'data' => $brands->map(fn($brand) => BrandTransformer::make($brand)
                ->linkSelfWithId()
                ->linkModels($vehicleTypeId)
                ->linkYears($vehicleTypeId)
            ),
            'links' => [
                'self' => request()->getUri(),
            ]
        ]);
    }

    public function show(string $vehicleTypeId, string $brandId, VehiclesClient $vehiclesClient): JsonResponse
    {
        $brands = $this->getBrands($vehicleTypeId, $vehiclesClient);
        return response()->json([
            'data' => $this->abortIfItemNotFound($brands, $brandId, BrandTransformer::class)
                ->linkSelf()
                ->linkModels($vehicleTypeId)
                ->linkYears($vehicleTypeId)
        ]);
    }

    private function getBrands(string $vehicleTypeId, VehiclesClient $vehiclesClient): Collection
    {
        return $this->validateTypeAndResponse($vehicleTypeId, fn() => $vehiclesClient->getBrands(
            $this->getLatestReferenceTableId(),
            $vehicleTypeId
        ));
    }
}
