<?php

namespace App\Http\Controllers\V1;

use App\Http\Clients\Fipe\VehiclesClient;
use App\Http\Controllers\Controller;
use App\Resources\External\ModelTransformer;
use App\Resources\External\YearTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

/**
 * Class VehicleTypeBrandModelsController.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Http\Controllers\V1
 */
class VehicleTypeBrandModelYearsController extends Controller
{
    public function index(
        string         $vehicleTypeId,
        string         $brandId,
        string         $modelId,
        VehiclesClient $vehiclesClient
    ): JsonResponse
    {
        $years = $this->getYears(
            $vehicleTypeId,
            $vehiclesClient,
            $brandId,
            $modelId
        );

        return response()->json([
            'data' => $years->map(fn($year) => YearTransformer::make($year)->linkSelfWithId()),
            'links' => [
                'self' => request()->getUri(),
            ]
        ]);
    }

    public function show(
        string         $vehicleTypeId,
        string         $brandId,
        string         $modelId,
        string         $yearId,
        VehiclesClient $vehiclesClient
    ): JsonResponse
    {
        $years = $this->getYears(
            $vehicleTypeId,
            $vehiclesClient,
            $brandId,
            $modelId
        );

        return response()->json([
            'data' => $this->abortIfItemNotFound($years, $yearId, YearTransformer::class)
                ->linkSelf()
        ]);
    }

    protected function getYears(string $vehicleTypeId, VehiclesClient $vehiclesClient, string $brandId, string $modelId): Collection
    {
        return $this->validateTypeAndResponse($vehicleTypeId, fn() => $vehiclesClient->getModelYears(
            $this->getLatestReferenceTableId(),
            $vehicleTypeId,
            $brandId,
            $modelId
        ));
    }
}
