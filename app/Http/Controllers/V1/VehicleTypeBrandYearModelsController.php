<?php

namespace App\Http\Controllers\V1;

use App\Http\Clients\Fipe\VehiclesClient;
use App\Http\Controllers\Controller;
use App\Resources\External\ModelTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

/**
 * Class VehicleTypeBrandYearModelsController.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Http\Controllers\V1
 */
class VehicleTypeBrandYearModelsController extends Controller
{
    public function index(
        string         $vehicleTypeId,
        string         $brandId,
        string         $yearId,
        VehiclesClient $vehiclesClient
    ): JsonResponse
    {
        $models = $this->getModels(
            $vehicleTypeId,
            $brandId,
            $yearId,
            $vehiclesClient
        );

        return response()->json([
            'data' => $models->map(
                fn($model) => ModelTransformer::make($model)
                    ->linkSelfWithId()
                    ->link('price', route('vehicle-types.brands.years.models.price.show', [
                        'vehicleTypeId' => $vehicleTypeId,
                        'brandId' => $brandId,
                        'yearId' => $yearId,
                        'modelId' => $model['Value'],
                    ]))
            ),
            'links' => [
                'self' => request()->getUri()
            ]
        ]);
    }

    public function show(
        string         $vehicleTypeId,
        string         $brandId,
        string         $yearId,
        string         $modelId,
        VehiclesClient $vehiclesClient
    ): JsonResponse
    {
        $models = $this->getModels(
            $vehicleTypeId,
            $brandId,
            $yearId,
            $vehiclesClient
        );

        return response()->json([
            'data' => $this->abortIfItemNotFound($models, $modelId, ModelTransformer::class)
                ->linkSelf()
                ->link('price', route('vehicle-types.brands.years.models.price.show', [
                    'vehicleTypeId' => $vehicleTypeId,
                    'brandId' => $brandId,
                    'yearId' => $yearId,
                    'modelId' => $modelId,
                ]))
        ]);
    }

    private function getModels(
        string         $vehicleTypeId,
        string         $brandId,
        string         $yearId,
        VehiclesClient $vehiclesClient
    ): Collection
    {
        return $this->validateTypeAndResponse($vehicleTypeId, function () use (
            $yearId, $brandId, $vehicleTypeId, $vehiclesClient
        ) {
            [$modelYear, $fuelTypeId] = explode('-', $yearId);

            return $vehiclesClient->getModelsByYear(
                $this->getLatestReferenceTableId(),
                $vehicleTypeId,
                $brandId,
                $yearId,
                $fuelTypeId,
                $modelYear
            );
        });
    }
}
