<?php

namespace App\Http\Controllers\V1;

use App\Http\Clients\Fipe\VehiclesClient;
use App\Http\Controllers\Controller;
use App\Resources\External\ModelTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

/**
 * Class VehicleTypeBrandModelsController.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Http\Controllers\V1
 */
class VehicleTypeBrandModelsController extends Controller
{
    public function index(
        string         $vehicleTypeId,
        string         $brandId,
        VehiclesClient $vehiclesClient
    ): JsonResponse
    {
        $models = $this->getModels($vehicleTypeId, $brandId, $vehiclesClient)
            ->map(fn($model) => ModelTransformer::make($model)->linkSelfWithId());

        return response()->json([
            'data' => $models,
            'links' => [
                'self' => request()->getUri(),
            ]
        ]);
    }

    public function show(
        string         $vehicleTypeId,
        string         $brandId,
        string         $modelId,
        VehiclesClient $vehiclesClient
    ): JsonResponse
    {
        $models = $this->getModels($vehicleTypeId, $brandId, $vehiclesClient);

        return response()->json([
            'data' => $this->abortIfItemNotFound($models, $modelId, ModelTransformer::class)
                ->linkSelf()
        ]);
    }

    private function getModels(
        string         $vehicleTypeId,
        string         $brandId,
        VehiclesClient $vehiclesClient
    ): Collection
    {
        return $this->validateTypeAndResponse($vehicleTypeId, fn() => $vehiclesClient->getModels(
            $this->getLatestReferenceTableId(),
            $vehicleTypeId,
            $brandId
        ));
    }
}
