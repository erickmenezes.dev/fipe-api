<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Traits\InteractsWithVehicles;
use Illuminate\Http\JsonResponse;

/**
 * Class VehicleTypeBrandYearModelPriceController.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Http\Controllers\V1
 */
class VehicleTypeBrandYearModelPriceController extends Controller
{
    use InteractsWithVehicles;

    public function __invoke(
        string         $vehicleTypeId,
        string         $brandId,
        string         $yearId,
        string         $modelId,
    ): JsonResponse
    {
        return response()->json([
            'data' => $this->getVehiclePrice(
                $vehicleTypeId,
                $brandId,
                $yearId,
                $modelId,
            ),
            'links' => [
                'self' => request()->getUri(),
            ]
        ]);
    }
}
