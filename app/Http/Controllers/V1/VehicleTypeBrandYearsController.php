<?php

namespace App\Http\Controllers\V1;

use App\Http\Clients\Fipe\VehiclesClient;
use App\Http\Controllers\Controller;
use App\Resources\External\YearTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

/**
 * Class VehicleTypeBrandModelsController.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Http\Controllers\V1
 */
class VehicleTypeBrandYearsController extends Controller
{
    public function index(
        string         $vehicleTypeId,
        string         $brandId,
        VehiclesClient $vehiclesClient
    ): JsonResponse
    {
        $models = $this->getYears($vehicleTypeId, $brandId, $vehiclesClient);

        return response()->json([
            'data' => $models->map(
                fn($model) => YearTransformer::make($model)
                    ->linkSelfWithId()
                    ->link('models', route('vehicle-types.brands.years.models.index', [
                        'vehicleTypeId' => $vehicleTypeId,
                        'brandId' => $brandId,
                        'yearId' => $model['Value']
                    ]))
            ),
            'links' => [
                'self' => request()->getUri()
            ]
        ]);
    }

    public function show(
        string         $vehicleTypeId,
        string         $brandId,
        string         $yearId,
        VehiclesClient $vehiclesClient
    ): JsonResponse
    {
        $models = $this->getYears($vehicleTypeId, $brandId, $vehiclesClient);

        return response()->json([
            'data' => $this->abortIfItemNotFound($models, $yearId, YearTransformer::class)
                ->linkSelf()
                ->link('models', route('vehicle-types.brands.years.models.index', [
                    'vehicleTypeId' => $vehicleTypeId,
                    'brandId' => $brandId,
                    'yearId' => $yearId
                ]))
        ]);
    }

    private function getYears(
        string         $vehicleTypeId,
        string         $brandId,
        VehiclesClient $vehiclesClient
    ): Collection
    {
        return $this->validateTypeAndResponse($vehicleTypeId, fn() => $vehiclesClient->getYears(
            $this->getLatestReferenceTableId(),
            $vehicleTypeId,
            $brandId
        ));
    }
}
