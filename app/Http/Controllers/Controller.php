<?php

namespace App\Http\Controllers;

use App\Http\Clients\Fipe\VehiclesClient;
use App\Resources\External\AbstractTransformer;
use Illuminate\Support\Collection;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function abortIfNothingFound(Collection $collection): void
    {
        if (
            $collection->has('codigo') &&
            $collection->has('erro') &&
            $collection['codigo'] === "0" &&
            $collection['erro'] === 'nadaencontrado'
        ) {
            abort(404);
        }
    }

    protected function abortIfInvalidVehicleType(string $type): void
    {
        if (!in_array($type, ['1', '2', '3'])) {
            abort(404, 'Vehicle type not found.');
        }
    }

    /**
     * @param \Illuminate\Support\Collection $items
     * @param mixed                          $id
     * @param class-string<T>                $transformerName
     *
     * @return T
     * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
     * @template T of \App\Resources\External\AbstractTransformer
     */
    protected function abortIfItemNotFound(Collection $items, mixed $id, string $transformerName): AbstractTransformer
    {
        $item = $items->where($transformerName::$primaryKey, $id)
            ->map(fn($brand) => $transformerName::make($brand))
            ->first();

        if (is_null($item)) {
            abort(404);
        }

        return $item;
    }

    protected function validateTypeAndResponse(string $vehicleTypeId, \Closure $closure): Collection
    {
        $this->abortIfInvalidVehicleType($vehicleTypeId);

        $items = collect($closure());

        $this->abortIfNothingFound($items);

        return $items;
    }

    protected function getLatestReferenceTableId(): int
    {
        return request()->query(
            'referenceTableId',
            app(VehiclesClient::class)->getReferenceTable()[0]['Codigo']
        );
    }
}
