<?php

namespace App\Providers;

use App\Http\Clients\Fipe\VehiclesClient;
use Illuminate\Support\ServiceProvider;
use Waffler\Waffler\Client\Factory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(VehiclesClient::class, fn() => Factory::make(
            VehiclesClient::class,
            ['base_uri' => 'https://veiculos.fipe.org.br/api/veiculos/']
        ));
    }
}
