<?php

namespace App\Resources\External;

/**
 * Class BrandTransformer.
 *
 * @property string $name
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Resources\External
 */
class BrandTransformer extends AbstractTransformer
{
    public static string $primaryKey = 'Value';

    protected function getAttributeMap(): array
    {
        return [
            'Label' => 'name',
        ];
    }

    public function linkModels(string $vehicleTypeId): static
    {
        return $this->link('models', route('brands.models.index', [
            'vehicleTypeId' => $vehicleTypeId,
            'brandId' => $this->getKey(),
        ]));
    }

    public function linkYears(string $vehicleTypeId): static
    {
        return $this->link('years', route('vehicle-types.brands.years.index', [
            'vehicleTypeId' => $vehicleTypeId,
            'brandId' => $this->getKey(),
        ]));
    }
}
