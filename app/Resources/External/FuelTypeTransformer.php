<?php

namespace App\Resources\External;

/**
 * Class FuelType.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Resources\External
 */
class FuelTypeTransformer extends AbstractTransformer
{
    // Não existe endpoint para esse tipo na api do fipe, assim como o vehicle types.

    protected function getAttributeMap(): array
    {
        return [
            'fuelTypeName' => 'name',
            'fuelTypeAcronym' => 'acronym'
        ];
    }
}
