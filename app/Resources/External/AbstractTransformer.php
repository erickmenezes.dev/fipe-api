<?php

namespace App\Resources\External;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class AbstractTransformer.
 *
 * @property int|string $id
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Resources\External
 */
abstract class AbstractTransformer implements Arrayable, Jsonable, \JsonSerializable
{
    public string $type;

    private static array $typeCache = [];

    protected static string $primaryKey = 'id';

    protected array $visible = ['*'];

    protected array $hidden = [];

    protected array $attributes = [];

    protected array $appends = [];

    protected array $links = [];

    protected array $meta = [];

    protected Collection $relations;

    private false|string|int $linkSelf = false;

    public function __construct(public array|Arrayable $data)
    {
        $this->data = is_array($this->data) ? $this->data : $this->data->toArray();

        $this->type ??= self::$typeCache[static::class] ??= Str::remove('Transformer', class_basename(static::class));

        $this->relations = new Collection([]);

        $this->initAttributes();

        $this->afterConstruct();
    }

    abstract protected function getAttributeMap(): array;

    public function toArray()
    {
        $attributes = $this->attributes;

        if (!in_array('*', $this->visible)) {
            $attributes = Arr::only($attributes, $this->visible);
        }

        foreach ($this->appends as $append) {
            $attributes[$append] = $this->{$this->hasAttributeAccessor($append)}();
        }

        $data = [
            'type' => $this->type,
            'id' => $this->getKey(),
            'attributes' => $attributes,
        ];

        if ($this->relations->isNotEmpty()) {
            $data['relationships'] = $this->relations;
        }

        if (!empty($this->links)) {
            $data['links'] = $this->links;
        }

        if (!empty($this->meta)) {
            $data['meta'] = $this->meta;
        }

        return $data;
    }

    public function hasAttributeAccessor(string $name, string $type = 'get'): false|string
    {
        $methodName = $type . Str::studly($name) . 'Attribute';

        if (!method_exists($this, $methodName)) {
            return false;
        }

        return $methodName;
    }

    public function toJson($options = 0)
    {
        return json_encode($this, $options);
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function getKey(): mixed
    {
        return $this->data[static::$primaryKey];
    }

    public static function make(array|Arrayable $data): static
    {
        return new static($data);
    }

    public function linkSelf(string|int $path = ''): static
    {
        return $this->link('self', request()->getUri() . ($path ? "/$path" : ''));
    }

    public function meta(string $name, string|array $value): self
    {
        $this->meta[$name] = $value;

        return $this;
    }

    public function link(string $name, string $value): static
    {
        $this->links[$name] = $value;

        return $this;
    }

    public function linkSelfWithId(): self
    {
        return $this->linkSelf($this->getKey());
    }

    public function setVisible(string|array $fields): self
    {
        $this->visible = is_string($fields)
            ? explode(',', $fields)
            : $fields;

        return $this;
    }

    public function only(array $keys): array
    {
        return Arr::only($this->attributes, $keys);
    }

    public function __get(string $name): mixed
    {
        if ($getter = $this->hasAttributeAccessor($name)) {
            return $this->$getter();
        } elseif (isset($this->relations[$name])) {
            return $this->relations[$name];
        }

        return $this->attributes[$name] ?? null;
    }

    public function __set(string $name, $value): void
    {
        if ($setter = $this->hasAttributeAccessor($name, 'set')) {
            $this->$setter($name, $value);
            return;
        }

        $this->attributes[$name] = $value;
    }

    public function append(string|array $attribute): self
    {
        foreach (Arr::wrap($attribute) as $item) {
            $this->appends[] = $item;
        }

        return $this;
    }

    protected function afterConstruct(): void
    {
        //
    }

    /**
     * @param string                                      $name
     * @param \App\Resources\External\AbstractTransformer $relation
     *
     * @return $this
     * @author   ErickJMenezes <erickmenezes.dev@gmail.com>
     */
    public function addRelationship(string $name, self $relation): self
    {
        $this->relations[$name] = $relation;
        return $this;
    }

    private function initAttributes(): void
    {
        foreach ($this->getAttributeMap() as $originalName => $newPath) {
            data_set($this->attributes, $newPath, data_get($this->data, $originalName));
        }
    }
}
