<?php

namespace App\Resources\External;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Str;

/**
 * Class VehiclePrice.
 *
 * @property string $valueBRL
 * @property string $brandName
 * @property string $modelName
 * @property int    $modelYear
 * @property string $fuelTypeName
 * @property string $fuelTypeAcronym
 * @property int    $fuelTypeId
 * @property string $fipeCode
 * @property string $referenceMonth
 * @property int    $vehicleTypeId
 * @property string $queryDate
 * @property ?string $yearId
 * @property ?string $modelId
 * @property ?string $brandId
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Resources\External
 */
class VehiclePrice extends AbstractTransformer
{
    public static string $primaryKey = 'Autenticacao';

    protected array $appends = ['value'];

    public function getValueAttribute(): float
    {
        return (float)(string)Str::of($this->valueBRL)
            ->remove('R$ ')
            ->replace('.', '')
            ->replaceMatches('/\,(\d*)/', '.$1');
    }

    protected function getAttributeMap(): array
    {
        return [
            'Valor' => 'valueBRL',
            'Marca' => 'brandName',
            'Modelo' => 'modelName',
            'AnoModelo' => 'modelYear',
            'Combustivel' => 'fuelTypeName',
            'CodigoFipe' => 'fipeCode',
            'MesReferencia' => 'referenceMonth',
            'TipoVeiculo' => 'vehicleTypeId',
            'SiglaCombustivel' => 'fuelTypeAcronym',
            'DataConsulta' => 'queryDate',
            'brandId' => 'brandId',
            'modelId' => 'modelId',
            'yearId' => 'yearId',
        ];
    }

    protected function afterConstruct(): void
    {
        $this->setVisible([
            'valueBRL', 'value', 'fipeCode',
            'queryDate', 'vehicleTypeId'
        ]);

        if ($this->brandId) {
            $this->addRelationship('brand', new BrandTransformer([
                'Label' => $this->brandName,
                'Value' => $this->brandId,
            ]));
        }

        if ($this->modelId) {
            $this->addRelationship('model', new ModelTransformer([
                'Label' => $this->modelName,
                'Value' => $this->modelId,
            ]));
        }

        $this->addRelationship('vehicleType', new VehicleTypeTransformer([
            'id' => $this->vehicleTypeId
        ]));

        if ($this->yearId) {
            $this->addRelationship('year', new YearTransformer([
                'Label' => "{$this->modelYear} {$this->fuelTypeName}",
                'Value' => $this->yearId,
            ]));

            $fuelTypeId = (int)explode('-', $this->yearId)[1];
            $this->addRelationship(
                'fuelType',
                new FuelTypeTransformer(
                    $this->only(['fuelTypeName', 'fuelTypeAcronym']) + ['id' => $fuelTypeId]
                )
            );
        }
    }
}
