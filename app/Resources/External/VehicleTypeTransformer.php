<?php

namespace App\Resources\External;

/**
 * Class VehicleTypeTransformer.
 *
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Resources\External
 */
class VehicleTypeTransformer extends AbstractTransformer
{
    protected static array $fallbackMap = [
        1 => ['id' => 1, 'slug' => 'cars', 'name' => 'Carros'],
        2 => ['id' => 2, 'slug' => 'bikes', 'name' => 'Motos'],
        3 => ['id' => 3, 'slug' => 'trucks', 'name' => 'Caminhões'],
    ];

    public function __construct(array $data) {
        parent::__construct(self::$fallbackMap[$data['id']]);
    }

    protected function getAttributeMap(): array
    {
        return [
            'name' => 'name',
            'slug' => 'slug',
        ];
    }
}
