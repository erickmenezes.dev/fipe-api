<?php

namespace App\Resources\External;

/**
 * Class YearTransformer.
 *
 * @property string $name
 * @property int    $year
 * @property int    $fuelTypeId
 * @author  ErickJMenezes <erickmenezes.dev@gmail.com>
 * @package App\Resources\External
 */
class YearTransformer extends AbstractTransformer
{
    public static string $primaryKey = 'Value';

    protected array $appends = ['year', 'fuelTypeId'];

    public function getYearAttribute(): int
    {
        [$year] = explode('-', $this->getKey());

        return (int)$year;
    }

    public function getFuelTypeIdAttribute(): int
    {
        [, $fuelTypeId] = explode('-', $this->getKey());

        return (int)$fuelTypeId;
    }

    protected function getAttributeMap(): array
    {
        return [
            'Label' => 'name'
        ];
    }
}
