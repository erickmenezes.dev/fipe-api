<?php

use Laravel\Lumen\Routing\Router;

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', fn() => ['versions' => ['v1']]);

$router->group([
    'prefix' => 'v1',
    'namespace' => 'V1'
], function (Router $router) {
    $router->get('vehicle-types', 'VehicleTypeController@index');
    $router->group(['prefix' => 'vehicle-types/{vehicleTypeId}'], function (Router $router) {
        $router->get('brands', 'VehicleTypeBrandsController@index');
        $router->get('brands/{brandId}', [
            'as' => 'vehicle-types.brands.show',
            'uses' => 'VehicleTypeBrandsController@show'
        ]);
        $router->get('brands/{brandId}/models', [
            'as' => 'brands.models.index',
            'uses' => 'VehicleTypeBrandModelsController@index',
        ]);
        $router->get('brands/{brandId}/models/{modelId}', 'VehicleTypeBrandModelsController@show');
        $router->get('brands/{brandId}/models/{modelId}/years', 'VehicleTypeBrandModelYearsController@index');
        $router->get('brands/{brandId}/models/{modelId}/years/{yearId}', 'VehicleTypeBrandModelYearsController@show');
        // Redirect para a rota de preço.
        $router->get('brands/{brandId}/models/{modelId}/years/{yearId}/price', fn() => redirect(route(
            'vehicle-types.brands.years.models.price.show',
            [
                'vehicleTypeId' => request()->route('vehicleTypeId'),
                'brandId' => request()->route('brandId'),
                'yearId' => request()->route('yearId'),
                'modelId' => request()->route('modelId')
            ]
        )));
        $router->get('brands/{brandId}/models/{modelId}/prices', 'VehicleTypeBrandModelPriceController@index');
        $router->get('brands/{brandId}/years', [
            'uses' => 'VehicleTypeBrandYearsController@index',
            'as' => 'vehicle-types.brands.years.index',
        ]);
        $router->get('brands/{brandId}/years/{yearId}', [
            'as' => 'vehicle-types.brands.years.show',
            'uses' => 'VehicleTypeBrandYearsController@show',
        ]);
        $router->get('brands/{brandId}/years/{yearId}/models', [
            'as' => 'vehicle-types.brands.years.models.index',
            'uses' => 'VehicleTypeBrandYearModelsController@index'
        ]);
        $router->get('brands/{brandId}/years/{yearId}/models/{modelId}', [
            'as' => 'vehicle-types.brands.years.models.show',
            'uses' => 'VehicleTypeBrandYearModelsController@show'
        ]);
        $router->get('brands/{brandId}/years/{yearId}/models/{modelId}/price', [
                'as' => 'vehicle-types.brands.years.models.price.show',
                'uses' => 'VehicleTypeBrandYearModelPriceController'
        ]);
    });
});
